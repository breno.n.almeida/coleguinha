namespace HeapNo
{
    public class NodeHeap
    {
        public Object data;
        public NodeHeap root;
        public NodeHeap next;

    public NodeHeap(Object data, NodeHeap root, NodeHeap next)
        {
            this.data = data;
            this.root = root;
            this.next = root;
        }


        public NodeHeap insert(int data)
        {
            NodeHeap newNode = new NodeHeap(data, root, next);
            if (this.root == null)
            {
                this.root = newNode;
                return this.root;
            }
            else
            {
                NodeHeap temp = this.root;
                while (temp.next != null)
                {
                    temp = temp.next;
                }
                temp.next = newNode;
                return this.root;
            }
        }
    
        public bool isEmpty()
        {
            return this.root == null;
        }       

    }

    public class HeapTree
    {
        public HeapNo.NodeHeap root;

        public HeapTree()
        {
            this.root = null;
        }

        public void insert(int data)
        {
            if (this.root == null)
            {
                this.root = new NodeHeap(data, root.root, null);
            }
            else
            {
                this.root.insert(data);
            }
        }

        public bool isEmpty()
        {
            return this.root == null;
        }

        public object removeMin()
        {
            if (this.isEmpty())
            {
                throw new Exception("Heap is empty");
            }
            else
            {
                NodeHeap temp = this.root;
                NodeHeap min = this.root;
                NodeHeap prev = null;
                while (temp.next != null)
                {
                    if ((int)temp.next.data < (int)min.data)
                    {
                        min = temp.next;
                        prev = temp;
                    }
                    temp = temp.next;
                }
                if (prev == null)
                {
                    this.root = this.root.next;
                }
                else
                {
                    prev.next = min.next;
                }
                return min.data;
            }
        }

        public int size()
        {
            if (this.isEmpty())
            {
                return 0;
            }
            else
            {
                int size = 1;
                NodeHeap temp = this.root;
                while (temp.next != null)
                {
                    size++;
                    temp = temp.next;
                }
                return size;
            }
        }

        public object min()
        {
            if (this.isEmpty())
            {
                throw new Exception("Heap is empty");
            }
            else
            {
                NodeHeap temp = this.root;
                NodeHeap min = this.root;
                while (temp.next != null)
                {
                    if ((int)temp.next.data < (int)min.data)
                    {
                        min = temp.next;
                    }
                    temp = temp.next;
                }
                return min.data;
            }
        }
        public void print()
        {
            if (this.root == null)
            {
                Console.WriteLine("Heap is empty");
            }
            else
            {
                NodeHeap temp = this.root;
                while (temp != null)
                {
                    Console.WriteLine(temp.data);
                    temp = temp.next;
                }
            }
        }
    }

}